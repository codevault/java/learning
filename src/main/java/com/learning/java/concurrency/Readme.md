* Why do we need multi threading
  * To prevent our application from freezing while processing time consuming tasks
 
* Why do we need concurrent programming?
  * Since all the threads in a process share the same memory
  * we need concurrent programming to make sure that the date in memory is consistent

* Thread and Process
  * Process
    * process is an instance of a program execution
    * memory and resources are separate for each process
    * in java we can create processes with the ProcessBuilder class <- do not attempt to do this
  * Thread
    * thread is essentially a lightweight process
    * thread is a unit of execution within a given process
    * single process can have n number of threads
    * each thread in a process shares the same memory and resources
    * creating new threads required fewer resources than creating new processed

* Time Slicing Algorithm
  * Single Processor
    * the single processor handles thread#1 for a short amount of time and then thread#2 and then again thread#1 and so on...
    * ![Time Slicing Single Core](data/Time_Slicing_Single_Core.jpg)
  * Multiple Processors
    * both the threads can be executed in parallel
    * ![Time Slicing Multi Core](data/Time_Slicing_Multi_Core.jpg)
    * no need for time slicing algorithm if the threads are running in separate cores
    * if the number of threads is greater than the number of processors, then the time slicing algorithm will be used