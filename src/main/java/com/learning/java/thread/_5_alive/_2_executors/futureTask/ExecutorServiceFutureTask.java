package com.learning.java.thread._5_alive._2_executors.futureTask;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.*;

public class ExecutorServiceFutureTask {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newCachedThreadPool(new NamedThreadFactory());

        FutureTask<Integer> ft1 = new FutureTask<>(new CallableTimerTask(500));
        FutureTask<Integer> ft2 = new FutureTask<>(new RunnableLoopTask(), 500);
        FutureTask<?> ft3 = new FutureTask<>(new RunnableLoopTask(), null);

        executorService.execute(ft1);
        executorService.execute(ft2);
        executorService.execute(ft3);

        executorService.shutdown();

        try {
            System.out.println("FutureTask-0 Callable Result = " + ft1.get());
            System.out.println("FutureTask-10 Runnable Result = " + ft2.get());
            System.out.println("FutureTask-11 Runnable Result = " + ft3.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class NamedThreadFactory implements ThreadFactory {

        private static int count = 0;
        private static final String name = "PoolWorker-";

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, name + count++);
        }
    }

    private static class CallableTimerTask implements Callable<Integer> {

        private static int count = 0;
        private final int taskId;

        private int returnValue;
        private final long sleepInterval;

        public CallableTimerTask(long sleepInterval) {
            this.taskId = count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Integer call() throws Exception {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            returnValue = TimerUtil.fixedTimerTask(taskId, sleepInterval);
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return returnValue;
        }
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 10;
        private final int taskId;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            TimerUtil.randomTimedLoopTask(taskId, 500, 10);
        }
    }
}
