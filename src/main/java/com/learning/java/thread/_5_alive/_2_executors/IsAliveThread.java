package com.learning.java.thread._5_alive._2_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.*;

public class IsAliveThread {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newCachedThreadPool(new NamedThreadFactory());

        Future<?> f1 = executorService.submit(new RunnableLoopTask());
        FutureTask<?> ft1 = new FutureTask<>(new RunnableLoopTask(), null);
        executorService.execute(ft1);

        executorService.shutdown();

        for (int i = 0; i < 5; i++) {
            TimerUtil.FIXED_SLEEP.accept(250L);

            System.out.println("Is 'Future-0' done = " + f1.isDone());
            System.out.println("Is 'Future-1' done = " + ft1.isDone());
        }

        System.out.println("Retrieving results...");

        try {
            System.out.println("'Future-0' result = " + f1.get());
            System.out.println("'Future-1' result = " + ft1.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class NamedThreadFactory implements ThreadFactory {

        private static int count = 0;
        private static final String name = "PoolWorker-";

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, name + count++);
        }
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            TimerUtil.randomTimedLoopTask(taskId, 500, 10);
        }
    }
}
