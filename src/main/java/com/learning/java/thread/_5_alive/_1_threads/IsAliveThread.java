package com.learning.java.thread._5_alive._1_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

public class IsAliveThread {

    public static final String IS_ALIVE = "Is '%s' alive = %s%n";

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        Thread t1 = new Thread(new RunnableLoopTask(), "Thread-0");
        boolean t1IsAlive = t1.isAlive();
        System.out.printf(IS_ALIVE, t1.getName(), t1IsAlive);
        t1.start();

        while (true) {
            TimerUtil.FIXED_SLEEP.accept(500L);
            t1IsAlive = t1.isAlive();
            System.out.printf(IS_ALIVE, t1.getName(), t1IsAlive);
            if (!t1IsAlive) {
                break;
            }
        }

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            TimerUtil.randomTimedLoopTask(taskId, 500, 10);
        }
    }
}
