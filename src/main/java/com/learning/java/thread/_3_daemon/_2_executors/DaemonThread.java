package com.learning.java.thread._3_daemon._2_executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class DaemonThread {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool(new NamedDaemonThreadFactory());

        executorService.execute(new RunnableLoopTask(500));
        executorService.execute(new RunnableLoopTask(1000));

        executorService.shutdown();

        System.out.println("Main thread has finished.");
    }

    private static class NamedThreadFactory implements ThreadFactory {

        protected static int count = 0;
        private static final String name = "PoolWorker-";

        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, name + count++);
        }
    }

    private static class NamedDaemonThreadFactory extends NamedThreadFactory {

        @Override
        public Thread newThread(Runnable r) {
            Thread thread = super.newThread(r);
            if (count % 2 == 0) {
                thread.setDaemon(true);
            }
            return thread;
        }
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;
        private final int sleepInterval;

        public RunnableLoopTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            RunnableLoopTask.ThreadType threadType = Thread.currentThread().isDaemon() ? ThreadType.DAEMON : ThreadType.USER;
            System.out.printf("##### [%s, %s] <%s> STARTING...%n", threadName, threadType, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s, %s] <%s> Tick Tick - %s%n", threadName, threadType, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * sleepInterval));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s, %s] <%s> FINISHED.%n", threadName, threadType, taskId);
        }

        private enum ThreadType {
            USER, DAEMON
        }
    }
}
