package com.learning.java.thread._3_daemon._1_threads;

import java.util.concurrent.TimeUnit;

public class DaemonThread {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        Thread t1 = new Thread(new RunnableLoopTask(500));
        Thread t2 = new Thread(new RunnableLoopTask(1000));

        t2.setDaemon(true);

        t1.start();
        t2.start();

        System.out.println("Main thread has finished.");
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;
        private final int sleepInterval;

        public RunnableLoopTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            ThreadType threadType = Thread.currentThread().isDaemon() ? ThreadType.DAEMON : ThreadType.USER;
            System.out.printf("##### [%s, %s] <%s> STARTING...%n", threadName, threadType, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s, %s] <%s> Tick Tick - %s%n", threadName, threadType, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * sleepInterval));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s, %s] <%s> FINISHED.%n", threadName, threadType, taskId);
        }

        private enum ThreadType {
            USER, DAEMON
        }
    }
}
