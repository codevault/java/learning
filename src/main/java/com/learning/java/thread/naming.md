* How to get name of current thread?
  1. get instance of current thread using `Thread.currentThread()`
  2. get name of current thread using `thread.getName()`
  * code: [GetThreadName](_1_threads/GetThreadName.java)

* How to name thread during thread creation?
  1. using constructor `Thread(Runnable task, String name)`
  2. using `thread.setName(<thread_name>)` after the thread is created
     * where `<thread_name>` is the name we want to assign to the thread
  3. code: [DuringCreation](_1_threads/_1_DuringCreation.java)

* How to name thread after thread is started?
  * code: [AfterStarting](_1_threads/_2_AfterStarting.java)

* How to name thread from its task?
  1. get instance of current thread using `Thread.currentThread()`
  2. set name of the current thread using `thread.setName(<thread_name>)`
     * where `<thread_name>` is the name we want to assign to the thread
  * code: [FromRunnableTask](_1_threads/_3_FromRunnableTask.java)

> NOTE: 
> * do not rename any thread once it has started
> * do not name a thread from inside the task which it runs
> * if u have to rename any thread after it is started, 
>   make sure to extract the thread names using `Thread.currentThread().getName()` everytime u need to use it in ur code

* How to name threads created by Executor Service?
  1. naming thread from its task(same as above)
  2. specify thread-naming strategy when executor-service is created
    * create a class which implements the ThreadFactory interface
    * implement the newThread() method by creating a named thread and returning it
    * provide the ThreadFactory implementation to the executor-service
    * code: [DuringCreation](_2_executors/_1_DuringCreation.java)

* Default Names of Executor Threads
  * `pool-N-thread-M`
    * where N is the pool-sequence-number in the JVM. 
      Incremented every time a new executor-pool is created.
    * where M is the thread-sequence-number inside an executor-pool. 
      Incremented every time a new thread is created in the executor-pool.

> NOTE:
> * thread-sequence-number for threads created using threads api start from 0
> * pool-sequence-number and thread-sequence-number for threads created using executors api start from 1
