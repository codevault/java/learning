## Refactoring
* Completed -> creation, naming, daemon
* ToDo: returnValue

### Contents
* Thread Class
  * thread.run() -> public void run()
  * thread.start() -> public void start()
  * new Thread(Runnable task) -> public Thread(Runnable task)
  * new Thread(Runnable task, String name) -> public Thread(Runnable task, String name)
  * Thread.currentThread() -> public static native Thread currentThread()
  * thread.getName() -> public final String getName()
  * thread.setName(String name) -> public final synchronized void setName(String name)
  * thread.setDaemon(boolean on) -> public final void setDaemon(boolean on)
  * thread.isDaemon() -> public final boolean isDaemon()
* Runnable Interface
  * runnable.run() -> void run()
* ExecutorService Interface
  * executorService.execute(Runnable command) -> void execute(Runnable command);
  * executorService.shutdown() -> void shutdown()
  * executorService.close() -> default void close()
  * executorService.submit(Runnable task) -> Future<?> submit(Runnable task)
  * executorService.submit(Runnable task, T result) -> <T> Future<T> submit(Runnable task, T result)
  * executorService.submit(Callable<T> task) -> <T> Future<T> submit(Callable<T> task)
* Executors Class
  * Executors.newSingleThreadExecutor() -> public static ExecutorService newSingleThreadExecutor()
  * Executors.newCachedThreadPool() -> public static ExecutorService newCachedThreadPool()
  * Executors.newFixedThreadPool(int nThreads) -> public static ExecutorService newFixedThreadPool(int nThreads)
  * Executors.newCachedThreadPool(ThreadFactory threadFactory) -> public static ExecutorService newCachedThreadPool(ThreadFactory threadFactory)
* ThreadFactory Interface
  * threadFactory.newThread(Runnable r) -> Thread newThread(Runnable r)
* Callable<V> Interface
  * callable.call() -> V call() throws Exception
* Future<V> Interface
  * future.get() -> V get() throws InterruptedException, ExecutionException
  * future.isDone() -> public boolean isDone()
* FutureTask<V> Class
  * future.get() -> V get() throws InterruptedException, ExecutionException
  * future.isDone() -> public boolean isDone()
* CompletionService<V> Interface
  * completionService.submit(Callable<V> task) -> Future<V> submit(Callable<V> task)
  * completionService.submit(Runnable task, V result) -> Future<V> submit(Runnable task, V result)
  * completionService.take() -> Future<V> take() throws InterruptedException
* ExecutorCompletionService Class
  * new ExecutorCompletionService<>(Executor executor) -> public ExecutorCompletionService(Executor executor)

> volatile: if a variable is accessed by multiple threads it should be defined as volatile
> synchronized: if a variable which is accessed by multiple threads need to be updated, update operation should only be performed from inside a synchronized block
