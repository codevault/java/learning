# Task
* What is a task
  * The code inside the run() method is called a task

# Thread Creation
* There are 2 ways to create Threads
  * Extend Thread Class and override the run() method
  * Implement Runnable Class and implement the run() method - preferred approach

# Thread Running
* How to run threads using Thread API
  * Call the start() method on the Thread instance to run the task in a separate thread
  * Call the run() method on the Thread instance to run the task in the same thread <- just for understandin


## Executor API
* Single Thread Executor
* Cached Thread Executor
* Fixed Thread Executor

* How to create thread using threads api?
  1. extend thread class
     * code: [ExtendThreadClass](_1_threads/_1_0_ExtendThreadClass.java)
     * code: [SelfStartingThread](_1_threads/_1_1_SelfStartingThread.java)
  2. implement runnable interface
     * code: [ImplementRunnable](_1_threads/_2_0_ImplementRunnable.java)
       * preferred approach for thread creation
     * code: [SelfStartingRunnable](_1_threads/_2_1_SelfStartingRunnable.java)
       * use to create fire and forget threads, cannot retrieve instance of thread
     * code: [InlineRunnable](_1_threads/_2_2_InlineRunnable.java)

* How to create thread using executors api?
  1. single thread executor
     * only one thread is run at a time
     * used to run threads in sequence
     * code: [SingleThreadExecutor](_2_executors/_1_SingleThreadExecutor.java)
  2. cached thread executor
     * if no threads are available for tasks new threads are created
     * threads that have completed are cached and reused
     * code: [CachedThreadExecutor](_2_executors/_2_CachedThreadExecutor.java)
  3. fixed thread executor
     * no of threads in the thread pool are fixed
     * if no threads are available, tasks wait for the threads to be available
     * code: [FixedThreadExecutor](_2_executors/_3_FixedThreadExecutor.java)
