package com.learning.java.thread._2_naming._1_threads;

import java.util.concurrent.TimeUnit;

public class _1_DuringCreation {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        // thread 0
        new Thread(new RunnableLoopTask(), "Worker-0").start();

        // thread 1
        Thread t1 = new Thread(new RunnableLoopTask());
        t1.setName("Worker-1");
        t1.start();

        System.out.println("Main thread has finished.");
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        public RunnableLoopTask() {
            this.taskId = "TASK-" + count++;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", threadName, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }
}
