package com.learning.java.thread._2_naming._2_executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class _1_DuringCreation {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool(new NamedThreadFactory());

        for (int i = 0; i < 2; i++) {
            executorService.execute(new RunnableLoopTask());
        }
        executorService.shutdown();

        System.out.println("Main thread has finished.");
    }

    private static class NamedThreadFactory implements ThreadFactory {

        private static int count = 0;
        private static final String name = "PoolWorker-";

        // this runnable is generated and provided by the jvm internally
        // it is not the runnable task that we have submitted to the executor service
        @Override
        public Thread newThread(Runnable r) {
            return new Thread(r, name + count++);
        }
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        public RunnableLoopTask() {
            this.taskId = "TASK-" + count++;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", threadName, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }
}
