package com.learning.java.thread._2_naming._1_threads;

import com.learning.java.thread._0_helper.StringUtil;

import java.util.concurrent.TimeUnit;

public class _3_FromRunnableTask {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        // thread 0
        new Thread(new RunnableLoopTask()).start();

        // thread 1
        Thread t1 = new Thread(new RunnableLoopTask());
        t1.start();

        System.out.println("Main thread has finished.");
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int instanceNumber;
        private final String taskId;

        public RunnableLoopTask() {
            this.instanceNumber = count++;
            this.taskId = "TASK-" + instanceNumber;
        }

        private void rename() {
            Thread thread = Thread.currentThread();
            String name = "Worker-" + instanceNumber;
            System.out.printf(StringUtil.RENAMING_THREAD,
                    thread.getName(), thread.getName(), name);
            thread.setName(name);
        }

        @Override
        public void run() {
            System.out.printf("##### [%s] <%s> STARTING...%n", Thread.currentThread().getName(), taskId);
            rename();
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", Thread.currentThread().getName(), taskId);
        }
    }
}
