package com.learning.java.thread._2_naming._1_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.TimeUnit;

public class _2_AfterStarting {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        // thread 0
        new Thread(new RunnableLoopTask(), "Worker-0").start();

        // thread 1
        Thread t1 = new Thread(new RunnableLoopTask());
        t1.start();

        TimerUtil.FIXED_SLEEP.accept(1000L);

        String name = "Worker-1";
        System.out.printf(StringUtil.RENAMING_THREAD,
                Thread.currentThread().getName(), t1.getName(), name);
        t1.setName(name);

        System.out.println("Main thread has finished.");
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        public RunnableLoopTask() {
            this.taskId = "TASK-" + count++;
        }

        @Override
        public void run() {
            System.out.printf("##### [%s] <%s> STARTING...%n", Thread.currentThread().getName(), taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", Thread.currentThread().getName(), taskId);
        }
    }
}
