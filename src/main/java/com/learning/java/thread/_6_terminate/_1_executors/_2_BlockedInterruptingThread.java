package com.learning.java.thread._6_terminate._1_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.*;

public class _2_BlockedInterruptingThread {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newCachedThreadPool();

        RunnableLoopTask runnableLoopTask = new RunnableLoopTask();
        CallableLoopTask callableLoopTask = new CallableLoopTask();

        Future<?> f1 = executorService.submit(runnableLoopTask);
        Future<Integer> f2 = executorService.submit(callableLoopTask);

        executorService.shutdown();

        TimerUtil.FIXED_SLEEP.accept(3000L);

        f1.cancel(true);
        f2.cancel(true);

        try {
            f1.get();
        } catch (CancellationException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }
    }

    private static class CallableLoopTask implements Callable<Integer> {

        private static int count = 0;
        private final int taskId;

        public CallableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }
    }
}
