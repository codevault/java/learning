package com.learning.java.thread._6_terminate._0_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/*
using interrupt
  * public void interrupt() - usually called by another thread on the thread to be interrupted
    * Interrupts this thread.
  * public static boolean interrupted() - to be called from inside the interrupted thread
    * Tests whether the current thread has been interrupted.
    * The interrupted status of the thread is cleared by this method.
  * public boolean isInterrupted() - to be called by another thread on the interrupted thread
    * Tests whether this thread has been interrupted.
    * The interrupted status of the thread is unaffected by this method

how to interrupt a thread
  * invoke Thread.interrupt() to interrupt the thread
  * check for interrupted status from inside the thread using Thread.interrupted()
  * if interrupted status is true, then terminate the thread

how thread interrupt works
  * interrupting a thread sets the interrupted flag inside the thread
  * once the threads interrupted status id enquired, the interrupted flag is reset automatically
*/

public class _1_NonBlockingInterruptingThread {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        Thread t1 = new Thread(new RunnableComplexCalculationTask(), "Thread-0");
        t1.start();

        TimerUtil.FIXED_SLEEP.accept(3000L);

        System.out.printf(StringUtil.THREAD_INTERRUPTED, Thread.currentThread().getName(), 0);
        t1.interrupt();

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableComplexCalculationTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        private final int DATASET_SIZE = 100000;

        public RunnableComplexCalculationTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                doComplexCalculation();

                if (Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }
}
