package com.learning.java.thread._6_terminate._1_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/*
boolean cancel(boolean mayInterruptIfRunning)
    * to be called by the class holding the future reference
public static boolean interrupted()
    * to be called from inside the interrupted task
boolean isCancelled()
    * to be called on the Future outside the interrupted task

NOTE: if a thread is canceled before it starts, then it will not run
 */
public class _1_NonBlockingInterruptingThread {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        RunnableComplexCalculationTask runnableComplexCalculationTask = new RunnableComplexCalculationTask();
        CallableComplexCalculationTask callableComplexCalculationTask1 = new CallableComplexCalculationTask();
        CallableComplexCalculationTask callableComplexCalculationTask2 = new CallableComplexCalculationTask();

        Future<?> f1 = executorService.submit(runnableComplexCalculationTask);
        Future<Integer> f2 = executorService.submit(callableComplexCalculationTask1);
        Future<Integer> f3 = executorService.submit(callableComplexCalculationTask2);

        executorService.shutdown();

        TimerUtil.FIXED_SLEEP.accept(3000L);

        System.out.println("Future1 Cancellation Status: " + f1.cancel(true));
        System.out.println("Future2 Cancellation Status: " + f2.cancel(true));
        System.out.println("Future3 Cancellation Status: " + f3.cancel(true));

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableComplexCalculationTask implements Runnable {

        private static int count = 1;
        private final int taskId;

        private final int DATASET_SIZE = 100000;

        public RunnableComplexCalculationTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                doComplexCalculation();

                if (Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }

    private static class CallableComplexCalculationTask implements Callable<Integer> {

        private static int count = 2;
        private final int taskId;

        private final int DATASET_SIZE = 100000;

        public CallableComplexCalculationTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() throws Exception {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                doComplexCalculation();

                if (Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }
}
