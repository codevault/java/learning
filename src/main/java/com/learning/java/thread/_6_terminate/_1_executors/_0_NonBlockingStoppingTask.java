package com.learning.java.thread._6_terminate._1_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class _0_NonBlockingStoppingTask {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newCachedThreadPool();

        RunnableLoopTask runnableLoopTask = new RunnableLoopTask();
        CallableLoopTask callableLoopTask = new CallableLoopTask();

        executorService.execute(runnableLoopTask);
        executorService.submit(callableLoopTask);

        executorService.shutdown();

        TimerUtil.FIXED_SLEEP.accept(3000L);

        runnableLoopTask.cancel();
        callableLoopTask.cancel();

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 1;
        private final int taskId;

        private volatile boolean interrupted = false;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                synchronized (this) {
                    if (interrupted) {
                        System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                        break;
                    }
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        public void cancel() {
            System.out.printf(StringUtil.THREAD_INTERRUPTED, Thread.currentThread().getName(), taskId);
            synchronized (this) {
                this.interrupted = true;
            }
        }
    }

    private static class CallableLoopTask implements Callable<Integer> {

        private static int count = 2;
        private final int taskId;

        private volatile boolean interrupted = false;

        public CallableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() throws Exception {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));

                synchronized (this) {
                    if (interrupted) {
                        System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                        break;
                    }
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }

        public void cancel() {
            System.out.printf(StringUtil.THREAD_INTERRUPTED, Thread.currentThread().getName(), taskId);
            synchronized (this) {
                this.interrupted = true;
            }
        }
    }
}
