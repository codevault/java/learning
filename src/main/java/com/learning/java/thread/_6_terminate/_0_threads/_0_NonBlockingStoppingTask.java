package com.learning.java.thread._6_terminate._0_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

public class _0_NonBlockingStoppingTask {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        RunnableLoopTask runnableLoopTask = new RunnableLoopTask();
        new Thread(runnableLoopTask, "Thread-0").start();

        TimerUtil.FIXED_SLEEP.accept(3000L);

        runnableLoopTask.cancel();

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        private volatile boolean interrupted = false;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                TimerUtil.RANDOM_SLEEP.accept(500L);

                synchronized (this) {
                    if (interrupted) {
                        System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                        break;
                    }
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        public void cancel() {
            System.out.printf(StringUtil.THREAD_INTERRUPTED, Thread.currentThread().getName(), taskId);
            synchronized (this) {
                this.interrupted = true;
            }
        }
    }
}
