package com.learning.java.thread._6_terminate._0_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class _3_InterruptingThread {

    public static long delay = 1000L;

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        Thread t1 = new Thread(new RunnableTask());
        t1.start();
        Thread t2 = new Thread(new RunnableTask());
        t2.start();
        Thread t3 = new Thread(new RunnableTask());
        t3.start();
        Thread t4 = new Thread(new RunnableTask());
        t4.start();
        Thread t5 = new Thread(new RunnableTask());
        t5.start();

        TimerUtil.FIXED_SLEEP.accept(delay);

        t1.interrupt();
        t2.interrupt();
        t3.interrupt();
        t4.interrupt();
        t5.interrupt();

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        private final int DATASET_SIZE = 100000;
        private boolean interrupted = false;

        public RunnableTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * delay));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_INTERRUPTED, Thread.currentThread().getName(), taskId);
                    interrupted = true;
                }

                doComplexCalculation();

                if (interrupted || Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }
}
