package com.learning.java.thread._6_terminate._1_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/*

List<Runnable> ExecutorService.shutdownNow()
    * returns a list of tasks that were awaiting execution
    * blocked and unblocking code will be interrupted
boolean ExecutorService.awaitTermination(long timeout, TImeUnit unit)

Future.get() can throw 3 types of exceptions
    * CancellationException -> thrown when get is called on a canceled task
    * ExecutionException -> thrown when the task internally throws any exception
        * the internal exception is wrapped in ExecutionException and stored in the ExecutorService
    * InterruptedException -> thrown when the thread which has called Future.get() and is waiting is interrupted
 */
public class _3_TerminateAll {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        ExecutorService executorService = Executors.newCachedThreadPool();

        RunnableComplexCalculationTask runnableComplexCalculationTask = new RunnableComplexCalculationTask();
        RunnableLoopTask runnableLoopTask = new RunnableLoopTask();
        CallableComplexCalculationTask callableComplexCalculationTask = new CallableComplexCalculationTask();
        CallableLoopTask callableLoopTask = new CallableLoopTask();
        RogueRunnableLoopTask rogueRunnableLoopTask = new RogueRunnableLoopTask();
        UnhandledCallableLoopTask unhandledCallableLoopTask = new UnhandledCallableLoopTask();

        executorService.execute(runnableComplexCalculationTask);
        executorService.execute(runnableLoopTask);
        Future<Integer> f1 = executorService.submit(callableComplexCalculationTask);
        Future<Integer> f2 = executorService.submit(callableLoopTask);

        executorService.execute(rogueRunnableLoopTask);
        Future<Integer> f3 = executorService.submit(unhandledCallableLoopTask);

        TimerUtil.FIXED_SLEEP.accept(3000L);

        List<Runnable> runnableList = executorService.shutdownNow();

        System.out.println("All Threads Terminated: " +
                executorService.awaitTermination(1000, TimeUnit.MILLISECONDS));
//        System.out.println("All Threads Terminated: " +
//                executorService.awaitTermination(10000, TimeUnit.MILLISECONDS));

        System.out.println("Future1 Result: " + f1.get());
        System.out.println("Future2 Result: " + f2.get());
        try {
            System.out.println("Future3 Result: " + f3.get());
        } catch (ExecutionException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            // e.getCause().printStackTrace(new PrintWriter(sw));
            System.out.println(sw.toString());
        }


        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableComplexCalculationTask implements Runnable {

        private static int count = 1;
        private final int taskId;

        private final int DATASET_SIZE = 100000;

        public RunnableComplexCalculationTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                doComplexCalculation();

                if (Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }

    private static class CallableComplexCalculationTask implements Callable<Integer> {

        private static int count = 2;
        private final int taskId;

        private final int DATASET_SIZE = 100000;

        public CallableComplexCalculationTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() throws Exception {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);

                doComplexCalculation();

                if (Thread.interrupted()) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }

        private void doComplexCalculation() {
            for (int i = 0; i < 10; i++) {
                Collections.sort(generateDataSet());
            }
        }

        private List<Integer> generateDataSet() {
            List<Integer> intList = new ArrayList<>();
            Random random = new Random();

            for (int i = 0; i < DATASET_SIZE; i++) {
                intList.add(random.nextInt(DATASET_SIZE));
            }
            return intList;
        }
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        public RunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
        }
    }

    private static class CallableLoopTask implements Callable<Integer> {

        private static int count = 0;
        private final int taskId;

        public CallableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; ; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    break;
                }
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }
    }

    private static class RogueRunnableLoopTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        public RogueRunnableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public void run() {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; i < 25; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    System.out.printf(StringUtil.THREAD_SHUTDOWN, Thread.currentThread().getName(), taskId);
                    e.printStackTrace();
                }
            }
        }
    }

    private static class UnhandledCallableLoopTask implements Callable<Integer> {

        private static int count = 0;
        private final int taskId;

        public UnhandledCallableLoopTask() {
            this.taskId = count++;
        }

        @Override
        public Integer call() throws Exception {
            System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
            for (int i = 0; i < 25; i++) {
                System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
                // InterruptedException in taken care by the ExecutorService
                TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
            }
            System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
            return -1;
        }
    }
}
