> NOTE:
> * Values are returned from tasks and not threads
> * No language level support in threads api to return values from tasks

* How to return values from normal Threads(Blocking)?
  * implement runnable with field to store task result and task completion status
  * add getter method to wait for the task completion status and return the task result
  * task should compute and store the task result, update the task status and notify waiting threads
  * create and start the thread, then call the getter method
  * limitation: the thread outputs are returned in the order in which the getters are called and not in the order of task completion

* How to return values from normal Threads(Non Blocking)?
  * using observer design pattern pattern
    * generic interface with notify method with result as input argument
    * implement observer by giving a task id and the implementation for notify
  * implement runnable and pass in the observer as a constructor argument
  * once the task execution is completed call observer.notify(result) to pass the result
  * advantage: the thread outputs are returned in the order of task completion

* How to return values from Executor Threads(Blocking):
  * use Callable<T> instead of Runnable
  * limitation: the thread outputs are returned in the order in which the getters are called and not in the order of task completion

* How to return values from Executor Threads(Non Blocking):
  * using CompletionService
  * advantage: the thread outputs are returned in the order of task completion

* Runnable vs Callable in Executor
  * Runnable
    * ExecutorService.execute(Runnable r);
      * void Runnable.run()
        * cannot return value
        * cannot throw checked exception
    * Future<?> ExecutorService.submit(Runnable r); or Future<> ExecutorService.submit(Runnable r);
      * Future.get()
        * will return null
        * call to get() blocks till the task execution is completed
    * Future<T> ExecutorService.submit(Runnable r, T value);
      * T Future.get()
        * will return value
        * call to get() blocks till the task execution is completed
  * Callable
    * Future<T> ExecutorService.submit(Callable<T> c);
      * T Callable.call() throws exception
        * can return result T
        * can throw checked exception
      * T Future.get()
        * can be used to get the result
        * call to get() blocks till the task execution is completed and result is available

> Note: ExecutorService.submit() can take a Runnable or Callable<T> as input argument
