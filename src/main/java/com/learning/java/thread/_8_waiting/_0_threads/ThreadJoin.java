package com.learning.java.thread._8_waiting._0_threads;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

// Thread.isAlive()
// wait() and notify()
// join()

public class ThreadJoin {

    public static void main(String[] args) throws InterruptedException {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());


        Thread t1 = new Thread(new RunnableTimerTask(200));
        Thread t2 = new Thread(new RunnableTimerTask(100));
        Thread t3 = new Thread(new RunnableTimerTask(300));
        Thread t4 = new Thread(new RunnableTimerTask(400));

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        t1.join();
        System.out.printf(StringUtil.THREAD_JOINED, Thread.currentThread().getName(), t1.getName());
        t2.join();
        System.out.printf(StringUtil.THREAD_JOINED, Thread.currentThread().getName(), t2.getName());
        t3.join();
        System.out.printf(StringUtil.THREAD_JOINED, Thread.currentThread().getName(), t3.getName());
        t4.join();
        System.out.printf(StringUtil.THREAD_JOINED, Thread.currentThread().getName(), t4.getName());

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        private final long sleepInterval;

        public RunnableTimerTask(long sleepInterval) {
            this.taskId = count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            TimerUtil.randomTimedLoopTask(taskId, sleepInterval, 10);
        }
    }
}
