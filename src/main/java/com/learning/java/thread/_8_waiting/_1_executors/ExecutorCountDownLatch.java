package com.learning.java.thread._8_waiting._1_executors;

import com.learning.java.thread._0_helper.StringUtil;
import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// CountDownLatch
// void await()
// void countdown()

public class ExecutorCountDownLatch {

    public static void main(String[] args) {

        System.out.printf(StringUtil.MAIN_STARTING, Thread.currentThread().getName());

        CountDownLatch countDownLatch = new CountDownLatch(2);

        ExecutorService executorService = Executors.newCachedThreadPool();

        executorService.execute(new RunnableTimerTask(null));
        executorService.execute(new RunnableTimerTask(countDownLatch));
        executorService.execute(new RunnableTimerTask(countDownLatch));
        executorService.execute(new RunnableTimerTask(null));

        executorService.shutdown();

        try {
            countDownLatch.await();
            System.out.printf("----- [%s] GOT THE SIGNAL TO CONTINUE...%n", Thread.currentThread().getName());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.printf(StringUtil.MAIN_FINISHED, Thread.currentThread().getName());
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final int taskId;

        private CountDownLatch countDownLatch;

        public RunnableTimerTask(CountDownLatch countDownLatch) {
            this.taskId = count++;
            this.countDownLatch = countDownLatch;
        }

        @Override
        public void run() {
            TimerUtil.randomTimedLoopTask(taskId, 500, 10);

            if (countDownLatch != null) {
                countDownLatch.countDown();
                System.out.printf("----- [%s] <TASK-%s> LATCH COUNT: %s%n",
                        Thread.currentThread().getName(), taskId, countDownLatch.getCount());
            }
        }
    }
}
