package com.learning.java.thread._9_scheduled_tasks._0_threads;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class RepeatedFixedRate {

    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    public static void main(String[] args) throws InterruptedException {
        System.out.println("--- Main Starting...");

        String threadName = Thread.currentThread().getName();
        Timer timer = new Timer("Timer-Thread", true);

        Date currentTime = new Date();

        long intervalMillis = 2000;
        Date scheduledTime = TimeUtils.getFutureTime(currentTime, 3000);
        timer.scheduleAtFixedRate(new MyTimerTask(500), scheduledTime, intervalMillis);
        System.out.printf("--- [%s] Task-0 CURRENT_TIME:%s SCHEDULED_TIME:%s REPEAT:%s---%n",
                threadName, dateFormat.format(currentTime), dateFormat.format(scheduledTime),
                TimeUtils.convertToFractionalSecondsStr(0, intervalMillis));

        long delayMillis = 4000;
        timer.scheduleAtFixedRate(new MyTimerTask(500), delayMillis, intervalMillis);
        System.out.printf("--- [%s] Task-0 CURRENT_TIME:%s DELAY:%s REPEAT:%s---%n",
                threadName, dateFormat.format(currentTime),
                TimeUtils.convertToFractionalSecondsStr(0, delayMillis),
                TimeUtils.convertToFractionalSecondsStr(0, intervalMillis));

        TimeUnit.MILLISECONDS.sleep(10000);
        timer.cancel();

        System.out.println("--- Main completed.");
    }

    private static class MyTimerTask extends TimerTask {

        private static int count = 0;
        private int instanceNumber;
        private String taskId;

        private final long sleepInterval;

        public MyTimerTask(long sleepInterval) {
            this.instanceNumber = count++;
            this.taskId = "MyTimerTask-" + instanceNumber;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {

            String threadName = Thread.currentThread().getName();
            Date scheduledTime = new Date(super.scheduledExecutionTime());

            Date startTime = new Date();
            System.out.printf("##### [%s] <%s> SCHEDULED_TIME:%s START_TIME:%s #####%n",
                    threadName, taskId, dateFormat.format(scheduledTime), dateFormat.format(startTime));

            try {
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            Date endTime = new Date();
            System.out.printf("##### [%s] <%s> END_TIME:%s DELAYED_BY:%s RUN_TIME:%s #####%n",
                    threadName, taskId, dateFormat.format(endTime),
                    TimeUtils.getTimeDifferenceInSeconds(scheduledTime, startTime),
                    TimeUtils.getTimeDifferenceInSeconds(startTime, endTime));
        }
    }

    private static class TimeUtils {

        private TimeUtils() {
        }

        public static Date getFutureTime(Date initialTime, long millisToAdd) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(initialTime.getTime() + millisToAdd);
            return cal.getTime();
        }

        public static String convertToFractionalSecondsStr(long seconds, long millis) {
            return new SecondsAndMillis(seconds, millis).toString();
        }

        public static SecondsAndMillis getTimeDifferenceInSeconds(Date start, Date end) {
            return new SecondsAndMillis(end.getTime() - start.getTime());
        }

        public static class SecondsAndMillis {

            private final String sign;
            private final long seconds;
            private final long millis;

            public SecondsAndMillis(long seconds, long millis) {
                this.sign = "";
                this.seconds = seconds;
                this.millis = millis;
            }

            public SecondsAndMillis(long millisDuration) {
                this.sign = (millisDuration < 0) ? "-" : "";
                this.seconds = Math.abs(millisDuration / 1000);
                this.millis = Math.abs(millisDuration % 1000);
            }

            @Override
            public String toString() {
                double secsNMillis = seconds + (millis / 1000.0);
                String secsStr = String.format("%.3f", secsNMillis);
                return sign + secsStr + " SECONDS";
            }
        }
    }
}
