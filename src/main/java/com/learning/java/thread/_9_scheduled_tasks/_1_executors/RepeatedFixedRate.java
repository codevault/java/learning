package com.learning.java.thread._9_scheduled_tasks._1_executors;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

public class RepeatedFixedRate {

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("HH:mm:ss.SSS");

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        System.out.println("--- Main Starting...");

        String threadName = Thread.currentThread().getName();


        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        Date currentTime = new Date();
        System.out.printf("--- [%s] CURRENT_TIME:%s---%n",
                threadName, DATE_FORMATTER.format(currentTime));

        ScheduledFuture<?> sf = executorService.scheduleAtFixedRate(
                new RunnableTimerTask(1000), 4, 2, TimeUnit.SECONDS);

//        for (int i = 0; i < 5; i++) {
//            Date scheduledTime = TimeUtils.getFutureTime(new Date(), sf.getDelay(TimeUnit.MILLISECONDS));
//            System.out.printf("--- Delay: %s%n", DATE_FORMATTER.format(scheduledTime));
//            TimeUnit.MILLISECONDS.sleep(3000);
//        }

        // sf.cancel(true);

        TimeUnit.MILLISECONDS.sleep(10500);
        System.out.println("Shutting down...");
        executorService.shutdown();

        System.out.println("--- Main completed.");
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        private final long sleepInterval;

        public RunnableTimerTask(long sleepInterval) {
            this.taskId = "RunnableTimerTask-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {

            String threadName = Thread.currentThread().getName();

            Date startTime = new Date();
            System.out.printf("##### [%s] <%s> START_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(startTime));

            try {
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            Date endTime = new Date();
            System.out.printf("##### [%s] <%s> END_TIME:%s RUN_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(endTime),
                    TimeUtils.getTimeDifferenceInSeconds(startTime, endTime));
        }
    }

    private static class TimeUtils {

        private TimeUtils() {
        }

        public static Date getFutureTime(Date initialTime, long millisToAdd) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(initialTime.getTime() + millisToAdd);
            return cal.getTime();
        }

        public static String convertToFractionalSecondsStr(long seconds, long millis) {
            return new SecondsAndMillis(seconds, millis).toString();
        }

        public static SecondsAndMillis getTimeDifferenceInSeconds(Date start, Date end) {
            return new SecondsAndMillis(end.getTime() - start.getTime());
        }

        public static class SecondsAndMillis {

            private final String sign;
            private final long seconds;
            private final long millis;

            public SecondsAndMillis(long seconds, long millis) {
                this.sign = "";
                this.seconds = seconds;
                this.millis = millis;
            }

            public SecondsAndMillis(long millisDuration) {
                this.sign = (millisDuration < 0) ? "-" : "";
                this.seconds = Math.abs(millisDuration / 1000);
                this.millis = Math.abs(millisDuration % 1000);
            }

            @Override
            public String toString() {
                double secsNMillis = seconds + (millis / 1000.0);
                String secsStr = String.format("%.3f", secsNMillis);
                return sign + secsStr + " SECONDS";
            }
        }
    }
}
