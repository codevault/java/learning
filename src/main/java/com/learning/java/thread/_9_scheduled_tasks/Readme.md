### Threads API
* Task Scheduling Classes and Concepts
  * Timer -> public class Timer -> java.util.Timer
    * spawns a thread for executing the tasks
    * constructors
      * public Timer()
      * public Timer(boolean isDaemon)
      * public Timer(String name)
      * public Timer(String name, boolean isDaemon)
    * contains the scheduling logic
      * execute a task once in the future
        * public void schedule(TimerTask task, long delay)
        * public void schedule(TimerTask task, Date time)
      * repeated fixed delay executions
        * public void schedule(TimerTask task, long delay, long period)
        * public void schedule(TimerTask task, Date firstTime, long period)
        * next execution is scheduled when the current one begins
        * each execution is scheduled relative to the actual start time of the previous one
        * start times drift forward over long runs
        * frequency of executions become lower than that specified, ie: executions per minute
        * appropriate for repeated activities that require smoothness, eg: blinking courser
      * repeated fixed rate execution
        * public void scheduleAtFixedRate(TimerTask task, long delay, long period)
        * public void scheduleAtFixedRate(TimerTask task, Date firstTime, long period)
        * next execution is scheduled when the current one begins
        * each execution is scheduled relative to the scheduled start time of the first execution
        * start times do not drift forward over long runs
        * frequency of execution remains constant
        * appropriate for running maintenance task at fixed times
      * terminate timer
        * public void cancel()
      * timer cleanup —> only used if absolutely required
        * public int purge()
  * TimerTask -> public abstract class TimerTask implements Runnable -> java.util.TimerTask
    * Timer class cannot run simple Runnable Tasks, it can only run TimerTask
    * TimerTask implements the Runnable Interface
    * tasks should be short lived for repeated execution
    * tasks should be created by extending TimerTask and implementing run()
    * methods
      * public abstract void run()
      * public boolean cancel() -> terminate timer task
      * public long scheduledExecutionTime()
  * NOTE
    * Single task-executor-thread per Timer object
    * Always call Timer.cancel() when application is shutdown, if timer is not daemon
      * can stop application from terminating
      * can cause memory leaks
    * Do not schedule any more tasks on he Timer after cancelling it, IllegalStateException will be thrown
    * Timer is thread-safe
    * Timer class does not offer any real-time guarantees
* Scheduling for One-Time Executive
* Fixed Delay Repeated Executions
* Fixed Rate Repeated Executions

### Executors API
* Important Interfaces and Classes
  * single thread scheduled executor
    * public static ScheduledExecutorService newSingleThreadScheduledExecutor()
  * scheduled thread pool
    * public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize)
  * public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit)
  * public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit)
    * Callable cannot be scheduled for repeated execution
  * public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit)
  * public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit);
  * only delay can be given, no exact time can be specified
  * ScheduledFuture
    * implements Future
    * implements Delayed
      * long getDelay(TimeUnit unit) - returned delay till next execution
        * some amount of error will be there - only use as reference
  * Runnable can be used here
* Scheduling for One-Time Executive
* Fixed Delay Repeated Executions
  * each execution is scheduled relative to the termination-time of the previous execution
    * in thread its start time
  * start times dript forward over long runs
  * frequency of execution becomes lower than that specified
  * if execution of a task is explicitly cancelled or if the execution enocountered an exeption
  * then all subsiquent executions of that task are suppressed
* Fixed Rate Repeated Executions
  * each execution is scheduled relative to the scheduled start time of the first execution
  * start times do not drift forward over long runs
  * hence frequency of executions remins constant