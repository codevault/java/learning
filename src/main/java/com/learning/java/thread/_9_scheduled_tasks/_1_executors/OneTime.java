package com.learning.java.thread._9_scheduled_tasks._1_executors;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;

public class OneTime {

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("HH:mm:ss.SSS");

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        System.out.println("--- Main Starting...");

        String threadName = Thread.currentThread().getName();


        // ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);

        Date currentTime = new Date();
        System.out.printf("--- [%s] CURRENT_TIME:%s---%n",
                threadName, DATE_FORMATTER.format(currentTime));

        ScheduledFuture<?> sf1 =  executorService.schedule(
                new RunnableTimerTask(3000), 2, TimeUnit.SECONDS);
        ScheduledFuture<Integer> sf2 =  executorService.schedule(
                new CallableTimerTask(100), 4, TimeUnit.SECONDS);

        executorService.shutdown();

        sf1.cancel(true);

        try {
            System.out.printf("--- [%s] Task-1 Result:%s---%n", threadName, sf1.get());
        } catch (CancellationException e) {
            e.printStackTrace();
        }

        System.out.printf("--- [%s] Task-2 Result:%s---%n", threadName, sf2.get());

        System.out.println("--- Main completed.");
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        private final long sleepInterval;

        public RunnableTimerTask(long sleepInterval) {
            this.taskId = "RunnableTimerTask-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {

            String threadName = Thread.currentThread().getName();

            Date startTime = new Date();
            System.out.printf("##### [%s] <%s> START_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(startTime));

            try {
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            Date endTime = new Date();
            System.out.printf("##### [%s] <%s> END_TIME:%s RUN_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(endTime),
                    TimeUtils.getTimeDifferenceInSeconds(startTime, endTime));
        }
    }

    private static class CallableTimerTask implements Callable<Integer> {

        private static int count = 0;
        private final String taskId;

        private final long sleepInterval;

        public CallableTimerTask(long sleepInterval) {
            this.taskId = "CallableTimerTask-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Integer call() throws Exception {

            String threadName = Thread.currentThread().getName();

            Date startTime = new Date();
            System.out.printf("##### [%s] <%s> START_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(startTime));

            try {
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            Date endTime = new Date();
            System.out.printf("##### [%s] <%s> END_TIME:%s RUN_TIME:%s #####%n",
                    threadName, taskId, DATE_FORMATTER.format(endTime),
                    TimeUtils.getTimeDifferenceInSeconds(startTime, endTime));

            return new Random().nextInt();
        }
    }

    private static class TimeUtils {

        private TimeUtils() {
        }

        public static Date getFutureTime(Date initialTime, long millisToAdd) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(initialTime.getTime() + millisToAdd);
            return cal.getTime();
        }

        public static String convertToFractionalSecondsStr(long seconds, long millis) {
            return new SecondsAndMillis(seconds, millis).toString();
        }

        public static SecondsAndMillis getTimeDifferenceInSeconds(Date start, Date end) {
            return new SecondsAndMillis(end.getTime() - start.getTime());
        }

        public static class SecondsAndMillis {

            private final String sign;
            private final long seconds;
            private final long millis;

            public SecondsAndMillis(long seconds, long millis) {
                this.sign = "";
                this.seconds = seconds;
                this.millis = millis;
            }

            public SecondsAndMillis(long millisDuration) {
                this.sign = (millisDuration < 0) ? "-" : "";
                this.seconds = Math.abs(millisDuration / 1000);
                this.millis = Math.abs(millisDuration % 1000);
            }

            @Override
            public String toString() {
                double secsNMillis = seconds + (millis / 1000.0);
                String secsStr = String.format("%.3f", secsNMillis);
                return sign + secsStr + " SECONDS";
            }
        }
    }
}
