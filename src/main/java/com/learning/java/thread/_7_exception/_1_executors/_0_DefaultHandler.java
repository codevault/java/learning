package com.learning.java.thread._7_exception._1_executors;

/*
How to handle exception in threads
    * Implement Thread.UncaughtExceptionHandler Interface
    * 3 Ways to use UncaughtExceptionHandler
        1. set as default handler for all the threads in the system
            static void Thread.serDefaultUncaughtExceptionHandler(UncaughtExceptionHandler eh)
*/

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class _0_DefaultHandler {

    public static void main(String[] args) throws InterruptedException {

        Thread.setDefaultUncaughtExceptionHandler(new ThreadExceptionHandler("DEFAULT_HANDLER"));

        ExecutorService executorService1 = Executors.newCachedThreadPool();

        executorService1.execute(new RunnableExceptionTask());
        executorService1.execute(new RunnableExceptionTask());

        ExecutorService executorService2 = Executors.newCachedThreadPool();

        executorService2.execute(new RunnableExceptionTask());
        executorService2.execute(new RunnableExceptionTask());

        executorService1.shutdown();
        executorService2.shutdown();
    }

    private static class RunnableExceptionTask implements Runnable {

        @Override
        public void run() {
            throw new RuntimeException();
        }
    }

    private static class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {

        private String handlerId;

        public ThreadExceptionHandler() {

        }

        public ThreadExceptionHandler(String handlerId) {
            this.handlerId = handlerId;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println(">>> " + this + " caught Exception in Thread - \"" +
                    t.getName() + "\" => " + e);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "@" + this.hashCode() +
                    (handlerId == null || "".equals(handlerId) ? "" : "(\"" + handlerId + "\")");
        }
    }
}
