package com.learning.java.thread._7_exception._1_executors;

/*
How to handle exception in threads
    * Implement Thread.UncaughtExceptionHandler Interface
    * 3 Ways to use UncaughtExceptionHandler
        1. set as default handler for all the threads in the system
            static void Thread.serDefaultUncaughtExceptionHandler(UncaughtExceptionHandler eh)
        2. set different handlers for different threads
            void Thread.setUncaughtExceptionHandler(UncaughtExceptionHandler eh
*/

import com.learning.java.thread._0_helper.TimerUtil;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public class _2_CombinationHandlers {

    public static void main(String[] args) throws InterruptedException {

        Thread.setDefaultUncaughtExceptionHandler(new ThreadExceptionHandler("DEFAULT_HANDLER"));

        ExecutorService executorService1 = Executors.newCachedThreadPool();

        executorService1.execute(new RunnableExceptionTask());
        executorService1.execute(new RunnableExceptionTask());

        ExecutorService executorService2 = Executors.newCachedThreadPool(new NamedExceptionNotifier());

        executorService2.execute(new RunnableExceptionTask());
        executorService2.execute(new RunnableExceptionTask());

        executorService1.shutdown();
        executorService2.shutdown();
    }

    private static class NamedExceptionNotifier implements ThreadFactory {

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setUncaughtExceptionHandler(new ThreadExceptionNotifier());
            return t;
        }
    }

    private static class RunnableExceptionTask implements Runnable {

        @Override
        public void run() {
            TimerUtil.FIXED_SLEEP.accept(100L);
            throw new RuntimeException();
        }
    }

    private static class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {

        private String handlerId;

        public ThreadExceptionHandler() {

        }

        public ThreadExceptionHandler(String handlerId) {
            this.handlerId = handlerId;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println(">>> " + this + " caught Exception in Thread - \"" +
                    t.getName() + "\" => " + e);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "@" + this.hashCode() +
                    (handlerId == null || "".equals(handlerId) ? "" : "(\"" + handlerId + "\")");
        }
    }

    private static class ThreadExceptionNotifier implements Thread.UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            notify(t.getName(), e);
        }

        public void notify(String threadName, Throwable e) {
            System.out.println("=> " + this + " notified => Thread - \"" +
                    threadName + "\" threw exception " + e);
        }
    }
}
