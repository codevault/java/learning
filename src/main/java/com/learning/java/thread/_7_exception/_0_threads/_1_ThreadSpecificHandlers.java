package com.learning.java.thread._7_exception._0_threads;


/*
How to handle exception in threads
    * Implement Thread.UncaughtExceptionHandler Interface
    * 3 Ways to use UncaughtExceptionHandler
        1. set as default handler for all the threads in the system
            static void Thread.serDefaultUncaughtExceptionHandler(UncaughtExceptionHandler eh)
        2. set different handlers for different threads
            void Thread.setUncaughtExceptionHandler(UncaughtExceptionHandler eh
*/

public class _1_ThreadSpecificHandlers {

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(new RunnableExceptionTask());
        t1.setUncaughtExceptionHandler(new ThreadExceptionHandler());

        Thread t2 = new Thread(new RunnableExceptionTask());
        t2.setUncaughtExceptionHandler(new ThreadExceptionNotifier());

        t1.start();
        t2.start();
    }

    private static class RunnableExceptionTask implements Runnable {

        @Override
        public void run() {
            throw new RuntimeException();
        }
    }

    private static class ThreadExceptionHandler implements Thread.UncaughtExceptionHandler {

        private String handlerId;

        public ThreadExceptionHandler() {

        }

        public ThreadExceptionHandler(String handlerId) {
            this.handlerId = handlerId;
        }

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            System.out.println(">>> " + this + " caught Exception in Thread - \"" +
                    t.getName() + "\" => " + e);
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "@" + this.hashCode() +
                    (handlerId == null || "".equals(handlerId) ? "" : "(\"" + handlerId + "\")");
        }
    }

    private static class ThreadExceptionNotifier implements Thread.UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread t, Throwable e) {
            notify(t.getName(), e);
        }

        public void notify(String threadName, Throwable e) {
            System.out.println("=> " + this + " notified => Thread - \"" +
                    threadName + "\" threw exception " + e);
        }
    }
}
