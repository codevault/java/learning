package com.learning.java.thread._1_creation._1_threads;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class _2_2_InlineRunnable {

    private static int count = 0;
    private static final Supplier<String> TASK_ID = () -> "TASK-" + count++;

    // useful runnable implementation
    // private static final Runnable RUNNABLE = () -> loopTask();
    // private static final Supplier<Runnable> RUNNABLE_SUPPLIER = () -> () -> loopTask();

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        // thread 0
        new Thread(() -> loopTask()).start();

        // thread 1
        Thread thread = new Thread(() -> loopTask());
        thread.start();
    }

    private static void loopTask() {
        String taskId = TASK_ID.get();
        String threadName = Thread.currentThread().getName();
        System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);
        for (int i = 0; i < 10; i++) {
            System.out.printf("[%s] <%s> Tick Tick - %s%n", threadName, taskId, i);
            try {
                TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
    }
}
