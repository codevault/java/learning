package com.learning.java.thread._1_creation._2_executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class _2_CachedThreadExecutor {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread is starting...");

        // new threads created: 3 -> Thread-0, Thread-1, Thread-2
        System.out.println("Submitting 3 tasks...");
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 3; i++) {
            executorService.execute(new RunnableLoopTask());
        }

        TimeUnit.MILLISECONDS.sleep(5000L);

        // cached threads used: 2 -> any 2 from Thread-0, Thread-1, Thread-2
        System.out.println("Submitting 2 more tasks...");
        for (int i = 0; i < 2; i++) {
            executorService.execute(new RunnableLoopTask());
        }

        TimeUnit.MILLISECONDS.sleep(5000L);

        // cached threads used: 3 -> Thread-0, Thread-1, Thread-2
        // new threads created: 2 -> Thread-4, Thread-5
        System.out.println("Submitting 5 more tasks...");
        for (int i = 0; i < 5; i++) {
            executorService.execute(new RunnableLoopTask());
        }

        executorService.shutdown();

        System.out.println("Main thread has finished.");
    }

    private static class RunnableLoopTask implements Runnable {

        private static int count = 1;
        private final String taskId;

        public RunnableLoopTask() {
            this.taskId = "TASK-" + count++;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", threadName, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }
}
