package com.learning.java.thread._1_creation._1_threads;

import java.util.concurrent.TimeUnit;

public class _2_1_SelfStartingRunnable {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        // thread 0
        new SelfStartingRunnableLoopTask();

        // thread 1
        new SelfStartingRunnableLoopTask();

        System.out.println("Main thread has finished.");
    }

    private static class SelfStartingRunnableLoopTask implements Runnable {

        private static int count = 0;
        private final String taskId;

        public SelfStartingRunnableLoopTask() {
            this.taskId = "TASK-" + count++;
            new Thread(this).start();   // self starting
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);
            for (int i = 0; i < 10; i++) {
                System.out.printf("[%s] <%s> Tick Tick - %s%n", threadName, taskId, i);
                try {
                    TimeUnit.MILLISECONDS.sleep((long) (Math.random() * 500));
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }
}
