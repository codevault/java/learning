package com.learning.java.thread._3_returnvalue._2_executors;

import java.util.concurrent.*;

public class _1_NonBlockingTechnique {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool();
        CompletionService<Result<String, Long>> completionService =
                new ExecutorCompletionService<>(executorService);

        completionService.submit(new CallableTimerTask(500));
        completionService.submit(new CallableTimerTask(1500));
        completionService.submit(new CallableTimerTask(1000));

        executorService.shutdown();

        for (int i = 0; i < 3; i++) {
            try {
                System.out.println(completionService.take().get());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("Main thread has finished.");
    }

    private static class Result<U, V> {

        private final U taskId;
        private final V result;

        public Result(U taskId, V result) {
            this.taskId = taskId;
            this.result = result;
        }

        public U getTaskId() {
            return taskId;
        }

        public V getResult() {
            return result;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "taskId=" + taskId +
                    ", result=" + result +
                    '}';
        }
    }

    private static class CallableTimerTask implements Callable<Result<String, Long>> {

        private static int count = 1;
        private final String taskId;
        private final int sleepInterval;
        private Result<String, Long> result;

        public CallableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Result<String, Long> call() throws Exception {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
            startTime = System.currentTimeMillis();
            TimeUnit.MILLISECONDS.sleep(sleepInterval);
            endTime = System.currentTimeMillis();
            System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);

            result = new Result<>(taskId, endTime - startTime);

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);

            return result;
        }
    }
}
