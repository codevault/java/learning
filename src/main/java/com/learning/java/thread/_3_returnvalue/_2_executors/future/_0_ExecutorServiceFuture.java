package com.learning.java.thread._3_returnvalue._2_executors.future;

import java.util.concurrent.*;

public class _0_ExecutorServiceFuture {

    private static int count = 1;

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<?> f1 = executorService.submit(new RunnableTimerTask(500));
        Future<Integer> f2 = executorService.submit(new RunnableTimerTask(1500), 1500);
        Future<Long> f3 = executorService.submit(new CallableTimerTask(1000));

        executorService.shutdown();

        try {
            System.out.println("Future-1 Runnable Result = " + f1.get());
            System.out.println("Future-2 Runnable Result = " + f2.get());
            System.out.println("Future-3 Callable Result = " + f3.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Main thread has finished.");
    }

    private static class RunnableTimerTask implements Runnable {

        private final String taskId;
        private final int sleepInterval;

        public RunnableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            try {
                System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
                System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }

    private static class CallableTimerTask implements Callable<Long> {

        private final String taskId;
        private final int sleepInterval;
        private long result;

        public CallableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Long call() throws Exception {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
            startTime = System.currentTimeMillis();
            TimeUnit.MILLISECONDS.sleep(sleepInterval);
            endTime = System.currentTimeMillis();
            System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);

            result = endTime - startTime;

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);

            return result;
        }
    }
}
