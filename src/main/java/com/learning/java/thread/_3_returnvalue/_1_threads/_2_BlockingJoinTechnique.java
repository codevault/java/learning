package com.learning.java.thread._3_returnvalue._1_threads;

import java.util.concurrent.TimeUnit;

public class _2_BlockingJoinTechnique {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread is starting...");

        RunnableTimerTask task0 = new RunnableTimerTask(500);
        Thread t0 = new Thread(task0);

        RunnableTimerTask task1 = new RunnableTimerTask(1500);
        Thread t1 = new Thread(task1);

        RunnableTimerTask task2 = new RunnableTimerTask(1000);
        Thread t2 = new Thread(task2);

        t0.start();
        t1.start();
        t2.start();

        t0.join();
        System.out.printf("----- [main] [%s] JOINED.%n", t0.getName());
        System.out.println("TASK-0 Result = " + task0.getResult());

        t1.join();
        System.out.printf("----- [main] [%s] JOINED.%n", t1.getName());
        System.out.println("TASK-1 Result = " + task1.getResult());

        t2.join();
        System.out.printf("----- [main] [%s] JOINED.%n", t2.getName());
        System.out.println("TASK-2 Result = " + task2.getResult());

        System.out.println("Main thread has finished.");
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final String taskId;
        private final int sleepInterval;
        private long result;

        public RunnableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            try {
                System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
                startTime = System.currentTimeMillis();
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
                endTime = System.currentTimeMillis();
                System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            result = endTime - startTime;

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }

        public long getResult() {
            return result;
        }
    }
}
