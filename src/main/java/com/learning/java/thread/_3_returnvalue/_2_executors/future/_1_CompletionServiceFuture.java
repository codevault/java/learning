package com.learning.java.thread._3_returnvalue._2_executors.future;

import java.util.concurrent.*;

public class _1_CompletionServiceFuture {

    private static int count = 1;

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool();
        CompletionService<Long> completionService =
                new ExecutorCompletionService<>(executorService);

        completionService.submit(new RunnableTimerTask(500), null);
        completionService.submit(new RunnableTimerTask(1500), 1500L);
        completionService.submit(new CallableTimerTask(1000));

        executorService.shutdown();

        for (int i = 0; i < 3; i++) {
            try {
                System.out.println("Future-? Result = " + completionService.take().get());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }

        System.out.println("Main thread has finished.");
    }

    private static class RunnableTimerTask implements Runnable {

        private final String taskId;
        private final int sleepInterval;

        public RunnableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            try {
                System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
                System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
        }
    }

    private static class CallableTimerTask implements Callable<Long> {

        private final String taskId;
        private final int sleepInterval;
        private long result;

        public CallableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Long call() throws Exception {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
            startTime = System.currentTimeMillis();
            TimeUnit.MILLISECONDS.sleep(sleepInterval);
            endTime = System.currentTimeMillis();
            System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);

            result = endTime - startTime;

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);

            return result;
        }
    }
}
