package com.learning.java.thread._3_returnvalue._2_executors;

import java.util.concurrent.*;

public class _0_BlockingTechnique {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Long> f1 = executorService.submit(new CallableTimerTask(500));
        Future<Long> f2 = executorService.submit(new CallableTimerTask(1500));
        Future<Long> f3 = executorService.submit(new CallableTimerTask(1000));

        executorService.shutdown();

        try {
            System.out.println("Future-1 Result = " + f1.get());
            System.out.println("Future-2 Result = " + f2.get());
            System.out.println("Future-3 Result = " + f3.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Main thread has finished.");
    }

    private static class CallableTimerTask implements Callable<Long> {

        private static int count = 1;
        private final String taskId;
        private final int sleepInterval;
        private long result;

        public CallableTimerTask(int sleepInterval) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
        }

        @Override
        public Long call() throws Exception {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
            startTime = System.currentTimeMillis();
            TimeUnit.MILLISECONDS.sleep(sleepInterval);
            endTime = System.currentTimeMillis();
            System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);

            result = endTime - startTime;

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);

            return result;
        }
    }
}
