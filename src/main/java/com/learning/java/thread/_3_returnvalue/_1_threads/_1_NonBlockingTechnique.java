package com.learning.java.thread._3_returnvalue._1_threads;

import java.util.concurrent.TimeUnit;

public class _1_NonBlockingTechnique {

    public static void main(String[] args) {
        System.out.println("Main thread is starting...");

        RunnableTimerTask task0 = new RunnableTimerTask(500, new ResultObserver());
        Thread t0 = new Thread(task0);

        RunnableTimerTask task1 = new RunnableTimerTask(1500, new ResultObserver());
        Thread t1 = new Thread(task1);

        RunnableTimerTask task2 = new RunnableTimerTask(1000, new ResultObserver());
        Thread t2 = new Thread(task2);

        t0.start();
        t1.start();
        t2.start();

        System.out.println("Main thread has finished.");
    }

    private interface IObserver<T> {

        void notify(String taskId, T result);
    }

    private static class ResultObserver implements IObserver<Long> {

        @Override
        public void notify(String taskId, Long result) {
            System.out.println(taskId + " Result = " + result);
        }
    }

    private static class RunnableTimerTask implements Runnable {

        private static int count = 0;
        private final String taskId;
        private final int sleepInterval;
        private long result;
        private final IObserver<Long> observer;

        public RunnableTimerTask(int sleepInterval, IObserver<Long> observer) {
            this.taskId = "TASK-" + count++;
            this.sleepInterval = sleepInterval;
            this.observer = observer;
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.printf("##### [%s] <%s> STARTING...%n", threadName, taskId);

            long startTime, endTime;
            try {
                System.out.printf("##### [%s] <%s> TIMER STARTED.%n", threadName, taskId);
                startTime = System.currentTimeMillis();
                TimeUnit.MILLISECONDS.sleep(sleepInterval);
                endTime = System.currentTimeMillis();
                System.out.printf("##### [%s] <%s> TIMER COMPLETED.%n", threadName, taskId);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            result = endTime - startTime;

            System.out.printf("***** [%s] <%s> FINISHED.%n", threadName, taskId);
            observer.notify(taskId, result);
        }
    }
}
