package com.learning.java.thread._0_helper;

public class StringUtil {

    public static final String MAIN_STARTING = "[%s] Main thread is starting...%n";
    public static final String MAIN_FINISHED = "[%s] Main thread has finished%n";

    public static final String THREAD_STARTING = "##### [%s] <TASK-%s> STARTING...%n";
    public static final String TASK_TICK_TICK = "[%s] <TASK-%s> Tick Tick - %s%n";
    public static final String THREAD_FINISHED = "***** [%s] <TASK-%s> FINISHED%n";

    public static final String RENAMING_THREAD = "----- [%s] Renaming thread [%s] -> [%s]%n";

    public static final String TASK_SLEEP_COMPLETED = "----- [%s] <TASK-%s> Sleep Completed%n";
    public static final String THREAD_NOTIFYING = "----- [%s] <TASK-%s> Notifying...%n";
    public static final String THREAD_WAITING = "----- [%s] <TASK-%s> Waiting...%n";
    public static final String THREAD_INTERRUPTED = "----- [%s] <TASK-%s> Interrupted%n";
    public static final String THREAD_SHUTDOWN = "----- [%s] <TASK-%s> Shutdown%n";
    public static final String THREAD_JOINED = "----- [%s] Joined [%s]%n";

    public static final String THREAD_STARTING_WITH_TYPE = "##### [%s, %s] <TASK-%s> STARTING...%n";
    public static final String THREAD_FINISHED_WITH_TYPE = "***** [%s, %s] <TASK-%s> FINISHED%n";
}
