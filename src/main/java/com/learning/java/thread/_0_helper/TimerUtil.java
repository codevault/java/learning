package com.learning.java.thread._0_helper;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class TimerUtil {

    private static void sleep(long ms) {
        try {
            TimeUnit.MILLISECONDS.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static final Consumer<Long> FIXED_SLEEP = TimerUtil::sleep;
    public static final Consumer<Long> RANDOM_SLEEP = l -> FIXED_SLEEP.accept((long) (Math.random() * l));

    public static void fixedTimedLoopTask(int taskId, long sleepInterval, int loopCount) {
        System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
        for (int i = 0; i < loopCount; i++) {
            System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
            FIXED_SLEEP.accept(sleepInterval);
        }
        System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
    }

    public static void randomTimedLoopTask(int taskId, long sleepInterval, int loopCount) {
        System.out.printf(StringUtil.THREAD_STARTING, Thread.currentThread().getName(), taskId);
        for (int i = 0; i < loopCount; i++) {
            System.out.printf(StringUtil.TASK_TICK_TICK, Thread.currentThread().getName(), taskId, i);
            RANDOM_SLEEP.accept(sleepInterval);
        }
        System.out.printf(StringUtil.THREAD_FINISHED, Thread.currentThread().getName(), taskId);
    }

    public static int fixedTimerTask(int taskId, long sleepInterval) {
        TimerUtil.FIXED_SLEEP.accept(sleepInterval);
        System.out.printf(StringUtil.TASK_SLEEP_COMPLETED, Thread.currentThread().getName(), taskId);
        return new Random().nextInt();
    }
}
